/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.kthdv;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author PHI
 */
@WebService(serviceName = "BookPrice")
public class BookPrice {


    /**
     * Web service operation
     * @param bookName
     * @return Double
     */
    @WebMethod(operationName = "getPrice")
    public Double getPrice(@WebParam(name = "bookName") String bookName) {
        //TODO write your implementation code here:
        switch(bookName){
            case "OOP": return 4.33;
            case "XML": return 2.00;
            case "KTHDV": return 3.00;
            default: return 5.00;
        }
        
    }
}
